#version 330 core

 uniform float particleSize = 0.2f;

 layout (points) in; // from vertex shader
 layout (triangle_strip, max_vertices = 4) out; // to fragment shader

 in Vertex {
	vec2 texCoord; //order matters! make sure to match the input and output order
	vec4 colour;
 } IN[];
 out Vertex {
	vec2 texCoord;
	vec4 colour;
 } OUT;
 
 void main() {
for(int i = 0; i < gl_in.length (); ++i ) {
 OUT.colour = IN[i].colour;
//topright
 gl_Position = gl_in[i].gl_Position;
 gl_Position.x += particleSize;
 gl_Position.y += particleSize;
 OUT.texCoord = vec2(1 ,0);
 EmitVertex ();
 gl_Position = gl_in[i].gl_Position;
 gl_Position.x -= particleSize;
 gl_Position.y += particleSize;
 OUT.texCoord = vec2(0 ,0);
 EmitVertex();
//bottomright
 gl_Position = gl_in[i].gl_Position;
 gl_Position.x += particleSize ;
 gl_Position.y -= particleSize ;
 OUT.texCoord = vec2(1 ,1);
 EmitVertex();
//bottomLeft
 gl_Position = gl_in[i].gl_Position;
 gl_Position.x -= particleSize;
 gl_Position.y -= particleSize;
 OUT.texCoord = vec2 (0 ,1);
 EmitVertex();

 EndPrimitive();
	}
 }
