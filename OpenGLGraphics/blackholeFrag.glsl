#version 150 core

uniform sampler2D blackhole;


uniform float time;


in Vertex	{
	vec2 texCoord;
	vec4 colour;
	//vec3 worldPos ;
} IN;

//In the tutorial notes, I've used gl_FragColor for the output variable. This is fine on the uni machines, but 
//doesn't play very nicely with some intel and amd graphics drivers, due to the gl_ prefix. For this reason,
//it's better to use a different variable name - so pick something sensible.
out vec4 fragColour;

void main(void)	{

	//float mod = 1/pow(2,time/2000);

	vec4 t1 = texture(blackhole, IN.texCoord);
	
	fragColour = t1;//t1;
	//fragColour = t1*t2;

}