#pragma once
#include "../nclgl/OGLRenderer.h"

#include "RenderObject.h"

#include <vector>

using std::vector;

class Renderer : public OGLRenderer	{
public:
	Renderer(Window &parent); //do note that
	//the Renderer class constructor calls the overridden constructor of the OGLRenderer base class
	~Renderer(void);

	virtual void	RenderScene();

	virtual void	Render(const RenderObject &o);

	virtual void	UpdateScene(float msec); //by default does nothing

	void	AddRenderObject(RenderObject &r) {
		renderObjects.push_back(&r);
	}

public:

	vector<RenderObject*> renderObjects;


	Mesh * quad;
	 Shader * smileyShader;
	 RenderObject smileyObject;
	 GLuint smileyTex;
	 GLuint staticTex;
	 float time;

	 Mesh * points;
	 Shader * pointShader;
	 RenderObject spritesObject;
	 GLuint diffuseTex;

	 Mesh * triangle;
	 Shader * triangleShader;
	 RenderObject  triangleObject;

	 GLuint LoadTexture(string filename);


};

