#version 150 core


 //uniform float time ;
 
 uniform float shrink;

in  vec2 texCoord;
in  vec4 colour;
in  vec3 position;

out Vertex	{
	vec2 texCoord;
	vec4 colour;
	vec3 raw_position;
} OUT;

vec3 TransformPosition(vec3 pos){
	
	return pos;
}


void main(void)	{
	OUT.raw_position=vec4(position,1.0f).xyz;
	
	gl_Position = vec4(position,1.0f);
	
	OUT.texCoord	= texCoord;
	OUT.colour		= colour;
}