#version 150 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform float time;

in  vec3 position;
in  vec2 texCoord;
in  vec4 colour;

out Vertex	{
	smooth vec2 texCoord;
	smooth vec4 colour;
	//smooth vec3 worldPos ;
} OUT;




vec4 Transform () {
 return projMatrix * viewMatrix * modelMatrix * vec4 ( position , 1);
 }



void main(void)	{
	//gl_Position		= (projMatrix * viewMatrix * modelMatrix) * vec4(position, 1.0); //made it to function
	
	
	
	//added stuff
	
	//float mod=sin(time);
	// vec4 worldPos = modelMatrix * vec4 ( position , 1.0);
	// worldPos . y += mod ;
	
	//gl_Position = projMatrix * viewMatrix * worldPos ;
	
    //vec4 clipPos = vec4 ( position , 1.0) ;
    //Transform ( clipPos , position , modelMatrix , viewMatrix , projMatrix );
	
	gl_Position = Transform (); //easiest way for the function
	
	
	
	//added stuff end
	
	OUT.texCoord	= texCoord;
	OUT.colour		= colour;
}