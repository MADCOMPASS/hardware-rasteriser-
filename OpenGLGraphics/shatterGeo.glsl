#version 150 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;


layout (triangles) in;
layout (triangle_strip, max_vertices=9) out;

 uniform float shrink;
 
 in Vertex{
	 
	 vec2 texCoord;
	 vec4 colour;
	 vec3 raw_position;
	 
 }IN[];
 
 out Vertex{
	 
	 vec2 texCoord;
	 vec4 colour;
	
 }OUT;
 
 
 
 void main(){
	 
	 vec3 A=IN[0].raw_position;
	 vec3 B=IN[1].raw_position;
	 vec3 C=IN[2].raw_position;
	 
	 vec3 centre=(A+B+C)/2;
	 
	 for(int i=0;i<gl_in.length();++i){
		 
		 OUT.colour=IN[i].colour; //to output normal colour and texCoord
		 
		 OUT.texCoord=IN[i].texCoord;
		 
		 gl_Position=gl_in[i].gl_Position + vec4(centre * shrink,0.0f); 
		 
		 gl_Position = projMatrix * viewMatrix * modelMatrix * gl_Position;
		 EmitVertex();
		  
	 }
	 EndPrimitive();
 }