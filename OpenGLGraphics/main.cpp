#include "Renderer.h"
#include "RenderObject.h"

#pragma comment(lib, "nclgl.lib")

void main(void) {
	Window w = Window(800, 600);
	Renderer r(w);
	//glDisable(GL_CULL_FACE); // to see transparency?
	glClearColor(0, 0, 0, 0);

	Mesh*	m	= Mesh::LoadMeshFile("cube.asciimesh");
	Shader* s	= new Shader("BasicVertCube.glsl", "BasicFragCube.glsl");

	Mesh*	m1 = Mesh::LoadMeshFile("cube.asciimesh");
	Shader* s1 = new Shader("BasicVertCube1.glsl", "BasicFragCube1.glsl");

	Mesh*	m2 = Mesh::LoadMeshFile("cube.asciimesh");
	Shader* s2 = new Shader("shatterVert.glsl", "shatterFrag.glsl", "shatterGeo.glsl");

	Mesh*	m3 = Mesh::LoadMeshFile("cube.asciimesh");
	Shader* s3 = new Shader("blackholeVert.glsl", "blackholeFrag.glsl");
	GLuint blackhole = r.LoadTexture("blackhole.png");
	
	

	if (s->UsingDefaultShader()) {
		cout << "Warning: Using default shader! Your shader probably hasn't worked..." << endl;
		cout << "Press any key to continue." << endl;
		std::cin.get();
	}

	RenderObject o(m, s);

	o.SetModelMatrix(Matrix4::Translation(Vector3(8,0,-10)) * Matrix4::Scale(Vector3(1,1,1)));

	RenderObject o1(m1, s1);

	o1.SetModelMatrix(Matrix4::Translation(Vector3(-8, 0, -10)) * Matrix4::Scale(Vector3(1, 1, 1)));

	RenderObject o2(m2, s2);

	o2.SetModelMatrix(Matrix4::Translation(Vector3(0, 6, -10)) * Matrix4::Scale(Vector3(1, 1, 1)));

	RenderObject o3(m3, s3);

	//o3.SetModelMatrix(Matrix4::Translation(Vector3(5, 0, -10)) * Matrix4::Scale(Vector3(1, 1, 1)));
	o3.SetTexture(blackhole);

	r.AddRenderObject(o);
	r.AddRenderObject(o1);
	r.AddRenderObject(o2);

	r.SetProjectionMatrix(Matrix4::Perspective(1, 100, 1.33f, 45.0f));

	r.SetViewMatrix(Matrix4::BuildViewMatrix(Vector3(0, 0, 10), Vector3(0, 0, -10)));

	
	

	while(w.UpdateWindow()) {



		if (Keyboard::KeyDown(KEY_B)) {

			
			r.AddRenderObject(o3);
		}

		float msec = w.GetTimer()->GetTimedMS();



		o.SetModelMatrix(o.GetModelMatrix() * Matrix4::Rotation(0.1f * msec,Vector3(1,0,1)));

		o1.SetModelMatrix(o1.GetModelMatrix() * Matrix4::Rotation(0.1f * msec, Vector3(1, 0, 1)));

		o2.SetModelMatrix(o2.GetModelMatrix() * Matrix4::Rotation(0.1f * msec, Vector3(1, 0, 1)));

		o3.SetModelMatrix(o3.GetModelMatrix() * Matrix4::Rotation(0.1f * msec, Vector3(0, 0, 1)));

		r.UpdateScene(msec);
		r.ClearBuffers();
		r.RenderScene();
		r.SwapBuffers();
	}
	delete m;
	delete s;
}