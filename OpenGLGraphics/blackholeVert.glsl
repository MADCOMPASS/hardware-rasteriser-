#version 150 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

 uniform float time ;

in  vec3 position;
in  vec2 texCoord;
in  vec4 colour;

out Vertex	{
	vec2 texCoord;
	vec4 colour;
} OUT;

void main(void)	{
	
	//float b= pow(2,time/8000);
	
float b=1/(time/2000);
	//position = b;
	//position=position-1;
	
	gl_Position		= (projMatrix * viewMatrix * modelMatrix) * vec4(position, b);
	
	OUT.texCoord	= texCoord;
	OUT.colour		= colour;
}