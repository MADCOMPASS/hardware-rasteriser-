#version 150 core

//added
 uniform sampler2D smileyTex ;//BindsmileyfacetextoaTUandset
 uniform sampler2D staticTex ;//BindstaticfacetextoaTUandset
 uniform float time ;//Thisshouldbesomesortofincrementingvalue






in Vertex	{
	smooth vec2 texCoord;
	smooth vec4 colour;
	//smooth vec3 worldPos ;
} IN;

//In the tutorial notes, I've used gl_FragColor for the output variable. This is fine on the uni machines, but 
//doesn't play very nicely with some intel and amd graphics drivers, due to the gl_ prefix. For this reason,
//it's better to use a different variable name - so pick something sensible.
out vec4 fragColour;

void main(void)	{	
//added
/*float r = sin( IN.worldPos.x) * 0.5 + 0.5;
float g = sin( IN.worldPos.y ) * 0.5 + 0.5;
float b = sin( IN.worldPos.z) * 0.5 + 0.5;

 fragColour.xyz = vec3 (r ,g , b );

 fragColour.a = 1.0;*/ //grey colour 
//added end

vec4 smileyColour = texture ( smileyTex , IN.texCoord );

if( smileyColour . b > 0.1) {
 vec2 tempTex = IN.texCoord ;
 tempTex.y += time ;

 fragColour = texture ( staticTex , tempTex );
 }
else{
 fragColour = smileyColour;
 }






	//fragColour = IN.colour;
}