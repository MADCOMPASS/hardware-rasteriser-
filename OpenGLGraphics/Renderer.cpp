#include "Renderer.h"

Renderer::Renderer(Window &parent) : OGLRenderer(parent)	{

	glEnable(GL_DEPTH_TEST); //delete it to set empty
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	 quad = Mesh::GenerateQuad();

	 smileyShader = new Shader("basicVert.glsl", "basicFrag.glsl");
	
	 smileyTex = LoadTexture("smiley.png");

	 staticTex = LoadTexture("noise.png");
	
     smileyObject= RenderObject(quad, smileyShader, smileyTex);

	 time = 0.0f;

	 renderObjects.push_back(&smileyObject);


	 points = Mesh::GeneratePoints(30);

	 pointShader = new Shader("PlanetVert.glsl", "PlanetFrag.glsl", "PlanetGeo.glsl");// keeep this order consistent , vert must be first, then frag and then geo and the rest of other things
	 
	 diffuseTex = LoadTexture("planet.png");
	 
	 spritesObject = RenderObject(points, pointShader, diffuseTex);
	 
	 renderObjects.push_back(&spritesObject);


	 triangle = Mesh::GenerateTriangle();

	 triangle->type = GL_PATCHES;

	 triangleShader = new Shader("tessVert.glsl", "tessFrag.glsl","", "tessTCS.glsl", "tessTES.glsl");

	 triangleObject = RenderObject(triangle, triangleShader);

	 renderObjects.push_back(&triangleObject);

	 triangleObject.SetModelMatrix(Matrix4::Translation(Vector3(0, -6, -10)) * Matrix4::Scale(Vector3(2, 2, 2)));

	 
	 projMatrix = Matrix4::Perspective(1.0f, 500.0f, width / height, 45.0f);
	 
	 spritesObject.SetModelMatrix(Matrix4::Translation(Vector3(0, 0, 3)) * Matrix4::Scale(Vector3(0.01f, 0.01f, 0.01f)));


}



GLuint Renderer::LoadTexture(string filename)
{

	GLuint tex = SOIL_load_OGL_texture(filename.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, tex);
	return tex;


}




Renderer::~Renderer(void)	{
	delete quad;
	delete smileyShader;
	 glDeleteTextures(1, &smileyTex);
	 glDeleteTextures(1, &staticTex);

}

void	Renderer::RenderScene() {

	if (Keyboard::KeyDown(KEY_V))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		Render(*(*i));  //delete for loop
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void	Renderer::Render(const RenderObject &o) {
	modelMatrix = o.GetWorldTransform();

	if(o.GetShader() && o.GetMesh()) {


		GLuint program = o.GetShader()->GetShaderProgram();

		glUseProgram(program);
		UpdateShaderMatrices(program);

		glUniform1f(glGetUniformLocation(program, "time"), time);

		//geo shape break
		glUniform1f(glGetUniformLocation(program, "shrink"), cos(time * 0.005f)+ 1.2f); // cos wave, makes the shape go back to a cube by adding 1.2f

		glUniform1i(glGetUniformLocation(program, "smileyTex"), 0);
		glUniform1i(glGetUniformLocation(program, "staticTex"), 1);
		glUniform1i(glGetUniformLocation(program, "diffuseTex"), 2);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, o.GetTexture());

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, staticTex);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, diffuseTex);

		o.GetMesh()->Draw();
	}

	for(vector<RenderObject*>::const_iterator i = o.GetChildren().begin(); i != o.GetChildren().end(); ++i ) {
		Render(*(*i));
	}
}

void	Renderer::UpdateScene(float msec) {
	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		(*i)->Update(msec);
	}

	time += msec;

}